package com.datajpa.app.springbootdatajpa.dao;

import com.datajpa.app.springbootdatajpa.models.entity.Cliente;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;

public interface IClienteDao extends PagingAndSortingRepository <Cliente, Long> {

    @Query("select c from Cliente c left join fetch c.facturas f where c.id=?1")
    public Optional<Cliente> fetchByIdWithFacturas(Long id);
}
