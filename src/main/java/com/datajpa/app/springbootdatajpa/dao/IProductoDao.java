package com.datajpa.app.springbootdatajpa.dao;

import com.datajpa.app.springbootdatajpa.models.entity.Producto;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface IProductoDao extends CrudRepository<Producto, Long> {

    @Query("select p from Producto p where p.nombre like %?1%")
    public List<Producto> findByNombre(String term);

    // This autogenerate the query with the name of the method
    // https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#repositories.query-methods.query-creation
    List<Producto> findByNombreLikeIgnoreCase(String term);
}
