package com.datajpa.app.springbootdatajpa.dao;

import com.datajpa.app.springbootdatajpa.models.entity.Usuario;
import org.springframework.data.repository.CrudRepository;

public interface IUsuarioDao extends CrudRepository<Usuario, Long> {
    Usuario findByUsername(String username);
}
