package com.datajpa.app.springbootdatajpa.models.entity.service;

import com.datajpa.app.springbootdatajpa.models.entity.Cliente;
import com.datajpa.app.springbootdatajpa.models.entity.Factura;
import com.datajpa.app.springbootdatajpa.models.entity.Producto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface IClienteService {

    public List<Cliente> findAll();

    public Page<Cliente> findAll(Pageable pageable);

    public void save(Cliente cliente);

    public Optional<Cliente> findOne(Long id);

    public Optional<Cliente> fetchByIdWithFacturas(Long id);

    public void delete(Long id);

    public List<Producto> findByNombre(String term);

    public void saveFactura(Factura factura);

    public Optional<Producto> findProductoById(Long id);

    public Factura findFacturaById(Long id);

    public void deleteFactura(Long id);

    public Factura fetchFacturaByIdWithClienteWithItemFacturaWithProducto(Long id);
}
