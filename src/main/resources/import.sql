/* Populate tables */

INSERT INTO `clientes` (id, nombre, apellido, email, create_at, foto)
VALUES (1, 'Andres', 'Guzman', 'AndresGuzman@fakemail.local', '2017-08-28', '');
INSERT INTO `clientes` (id, nombre, apellido, email, create_at, foto)
VALUES (2, 'John', 'Doe', 'JohnDoe@fakemail.local', '2017-08-28', '');
INSERT INTO `clientes` (id, nombre, apellido, email, create_at, foto)
VALUES (3, 'Joanna', 'Dao', 'JoannaDao@fakemail.local', '2017-08-28', '');
INSERT INTO `clientes` (id, nombre, apellido, email, create_at, foto)
VALUES (4, 'Lnius', 'Tovald', 'LinusTov@fakemail.local', '2017-08-28', '');
INSERT INTO `clientes` (id, nombre, apellido, email, create_at, foto)
VALUES (5, 'Jane', 'Dao', 'JaneDao@fakemail.local', '2017-08-28', '');
INSERT INTO `clientes` (id, nombre, apellido, email, create_at, foto)
VALUES (6, 'Milo', 'Lag', 'MiloLag@fakemail.local', '2017-08-28', '');
INSERT INTO `clientes` (id, nombre, apellido, email, create_at, foto)
VALUES (7, 'Ralph', 'Lauren', 'RalphLauren@fakemail.local', '2017-08-28', '');
INSERT INTO `clientes` (id, nombre, apellido, email, create_at, foto)
VALUES (8, 'Phillip', 'Dao', 'PhillipDao@fakemail.local', '2017-08-28', '');
INSERT INTO `clientes` (id, nombre, apellido, email, create_at, foto)
VALUES (9, 'Stephen', 'King', 'StephenKing@fakemail.local', '2017-08-28', '');
INSERT INTO `clientes` (id, nombre, apellido, email, create_at, foto)
VALUES (10, 'Charles', 'Darwin', 'CDarwin@fakemail.local', '2017-08-28', '');
INSERT INTO `clientes` (id, nombre, apellido, email, create_at, foto)
VALUES (11, 'Sam', 'Gamgee', 'SamGamgee@fakemail.local', '2017-08-28', '');
INSERT INTO `clientes` (id, nombre, apellido, email, create_at, foto)
VALUES (12, 'Meriadoc', 'Brandybuck', 'MBrandybuck@fakemail.local', '2017-08-28', '');
INSERT INTO `clientes` (id, nombre, apellido, email, create_at, foto)
VALUES (13, 'Peregrin', 'Tuck', 'PeregrinTuk@fakemail.local', '2017-08-28', '');
INSERT INTO `clientes` (id, nombre, apellido, email, create_at, foto)
VALUES (14, 'Frodo', 'Baggins', 'FrodoBaggins@fakemail.local', '2017-08-28', '');
INSERT INTO `clientes` (id, nombre, apellido, email, create_at, foto)
VALUES (15, 'Gandalf', 'TheGray', 'Gandalf@fakemail.local', '2017-08-28', '');
INSERT INTO `clientes` (id, nombre, apellido, email, create_at, foto)
VALUES (16, 'Steve', 'Rogers', 'SteveRogers@fakemail.local', '2017-08-28', '');
INSERT INTO `clientes` (id, nombre, apellido, email, create_at, foto)
VALUES (17, 'Tony', 'Stark', 'TonyStark@fakemail.local', '2017-08-28', '');
INSERT INTO `clientes` (id, nombre, apellido, email, create_at, foto)
VALUES (18, 'Thor', 'Odinson', 'ThorOdinson@fakemail.local', '2017-08-28', '');
INSERT INTO `clientes` (id, nombre, apellido, email, create_at, foto)
VALUES (19, 'Black', 'Widow', 'BlackWidow@fakemail.local', '2017-08-28', '');
INSERT INTO `clientes` (id, nombre, apellido, email, create_at, foto)
VALUES (20, 'Harry', 'Potter', 'HarryPotter@fakemail.local', '2017-08-28', '');
INSERT INTO `clientes` (id, nombre, apellido, email, create_at, foto)
VALUES (21, 'Sirius', 'Black', 'SiriusBlack@fakemail.local', '2017-08-28', '');
INSERT INTO `clientes` (id, nombre, apellido, email, create_at, foto)
VALUES (22, 'Severus', 'Snape', 'Snape@fakemail.local', '2017-08-28', '');
INSERT INTO `clientes` (id, nombre, apellido, email, create_at, foto)
VALUES (23, 'Phil', 'Collins', 'PhilCollins@fakemail.local', '2017-08-28', '');
INSERT INTO `clientes` (id, nombre, apellido, email, create_at, foto)
VALUES (24, 'Tadeo', 'Jhon', 'TadeoJhon@fakemail.local', '2017-08-28', '');
INSERT INTO `clientes` (id, nombre, apellido, email, create_at, foto)
VALUES (25, 'Darth', 'Vader', 'DarthVader@fakemail.local', '2017-08-28', '');

/* Populate tabla productos */
INSERT INTO `productos` (nombre, precio, create_at)
VALUES ('Panasonic Pantalla LCD', 249, NOW());
INSERT INTO `productos` (nombre, precio, create_at)
VALUES ('Sony Camara digital DSC-W320B', 289, NOW());
INSERT INTO `productos` (nombre, precio, create_at)
VALUES ('Apple iPod shuffle', 135, NOW());
INSERT INTO `productos` (nombre, precio, create_at)
VALUES ('Sony Notebook Z110', 540, NOW());
INSERT INTO `productos` (nombre, precio, create_at)
VALUES ('Hewlett Packard Multifuncional F2280', 46, NOW());
INSERT INTO `productos` (nombre, precio, create_at)
VALUES ('Bianchi Bicicleta Aro 26', 155, NOW());
INSERT INTO `productos` (nombre, precio, create_at)
VALUES ('Mica Comoda 5 Cajones', 270, NOW());

/* Creamos algunas facturas */
INSERT INTO `facturas` (descripcion, observacion, cliente_id, create_at)
VALUES ('Factura equipos de oficina', null, 1, NOW());
INSERT INTO `facturas_items` (cantidad, factura_id, producto_id)
VALUES (1, 1, 1);
INSERT INTO `facturas_items` (cantidad, factura_id, producto_id)
VALUES (2, 1, 4);
INSERT INTO `facturas_items` (cantidad, factura_id, producto_id)
VALUES (1, 1, 5);
INSERT INTO `facturas_items` (cantidad, factura_id, producto_id)
VALUES (1, 1, 7);

INSERT INTO `facturas` (descripcion, observacion, cliente_id, create_at)
VALUES ('Factura Bicicleta', 'Alguna nota importante!', 1, NOW());
INSERT INTO `facturas_items` (cantidad, factura_id, producto_id)
VALUES (3, 2, 6);

/* Creamos algunos usuarios con sus roles */

INSERT INTO `users` (username, password, enabled)
VALUES ('andres', '$2a$10$IsoeXb71wA6L8wpMmqo0Vu2GTJEnJxaGPA4fnzzZzS2BuxSflisI2', 1);
INSERT INTO `users` (username, password, enabled)
VALUES ('admin', '$2a$10$8XLOgltAn0fyZgnr2iLRa.HJbNfpTyCzIrIJSBJlN29k0r9fGZhzG', 1);

INSERT INTO `authorities` (user_id, authority)
VALUES (1, 'ROLE_USER');
INSERT INTO `authorities` (user_id, authority)
VALUES (2, 'ROLE_ADMIN');
INSERT INTO `authorities` (user_id, authority)
VALUES (2, 'ROLE_USER');